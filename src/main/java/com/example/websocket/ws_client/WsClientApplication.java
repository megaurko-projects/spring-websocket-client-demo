package com.example.websocket.ws_client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.websocket.Constants;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.*;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

@SpringBootApplication
public class WsClientApplication {
    private static final Logger LOG = LogManager.getLogger(WsClientApplication.class);
    private static final String WS_HOST = "wss://localhost:8443/ugs";

    public static void main(String[] args) {
        //runBasicSession(WS_HOST);
        try {
            runStompSession(WS_HOST);
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
            throw new RuntimeException(e);
        }
    }

    private static void runBasicSession(String url) {
        StandardWebSocketClient client = new StandardWebSocketClient();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + getCredentials().getBase64Authorization());
        WebSocketHttpHeaders headers = new WebSocketHttpHeaders(httpHeaders);

        try(WebSocketSession session = client.doHandshake(new MyHandler(), headers, new URI(url)).get()) {
			new Scanner(System.in).nextLine(); //Don't close immediately.
		} catch (InterruptedException | ExecutionException | IOException | URISyntaxException e) {
			LOG.error(e.getMessage(), e);
		}
    }

    private static void runStompSession(String url)
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        StandardWebSocketClient client = new StandardWebSocketClient();

        //TODO: SELF SIGNED STRATEGY ISN'T SUITABLE FOR PRODUCTION!!!!
        SSLContext sslContext = SSLContextBuilder.create()
                .loadTrustMaterial(null, TrustSelfSignedStrategy.INSTANCE)
                .build();
        client.setUserProperties(new HashMap<String, Object>() {{ put(Constants.SSL_CONTEXT_PROPERTY, sslContext); }});

        WebSocketStompClient stompClient = new WebSocketStompClient(client);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSession session = null;
        // put password here, this one is automatically generated password from spring security
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + getCredentials().getBase64Authorization());

        WebSocketHttpHeaders headers = new WebSocketHttpHeaders(httpHeaders);
        try {
            session = stompClient.connect(url, headers, new StompHandler()).get();
            while (true) {
                session.send("/app/queue/message", new RequestMsg("Hello!"));
                Thread.sleep(5000);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            Optional.ofNullable(session)
                    .ifPresent(StompSession::disconnect);
        }
    }

    private static Credentials getCredentials() {
        return new Credentials("admin", "password");
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Credentials {
        private String username;
        private String password;

        public String getPlainAuthorization() {
            return username + ":" + password;
        }

        public String getBase64Authorization() {
            return new String(Base64.getEncoder().encode(getPlainAuthorization().getBytes()));
        }
    }

    @Data
    @ToString
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RequestMsg {
        private String msg;
    }


    private static class StompHandler extends StompSessionHandlerAdapter {

        @Override
        public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
            super.afterConnected(session, connectedHeaders);
            session.subscribe("/dst/broadcast", this);
            session.subscribe("/user/response/private", this);
        }

        @Override public Type getPayloadType(StompHeaders headers) {
            return RequestMsg.class;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload) {
            LOG.info(payload.toString());
        }

        @Override public void handleException(
                StompSession session,
                StompCommand command,
                StompHeaders headers,
                byte[] payload,
                Throwable exception
        ) {
            LOG.error(exception.getMessage());
        }

        @Override
        public void handleTransportError(StompSession session, Throwable exception) {
            LOG.error(exception.getMessage());
        }
    }

    private static class MyHandler implements WebSocketHandler {

        @Override public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {

        }

        @Override public void handleMessage(
                WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage
        ) throws Exception {
            LOG.info(webSocketMessage.getPayload());
        }

        @Override public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable)
                throws Exception {
            LOG.error(throwable.getMessage());
        }

        @Override public void afterConnectionClosed(
                WebSocketSession webSocketSession, CloseStatus closeStatus
        ) throws Exception {

        }

        @Override public boolean supportsPartialMessages() {
            return false;
        }
    }
}
